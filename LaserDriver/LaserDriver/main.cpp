#include "LaserServer.h"


#include <wiringPi.h>
#include <pca9685servo.h>

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include <iostream>

#define	LED	17

int main(void)
{
	if (getuid() != 0) {
		fprintf(stderr, "Program is not started as \'root\' (sudo)\n");
		return -1;
	}

	wiringPiSetupSys();

	pinMode(LED, OUTPUT);

	LaserServer server{6000};
	PCA9685Servo servo;

	//i dont now what this do :P
	servo.SetLeftUs(700);      
	servo.SetRightUs(2400);
	servo.Dump();

	servo.SetAngle(CHANNEL(0), ANGLE(90));
	servo.SetAngle(CHANNEL(1), ANGLE(90));

	while (true)
	{
		auto msg = server.popRequest();	
		if (!msg.empty())
		{
			if (msg.find("x") != std::string::npos)
			{
				int opt;
				int ret = sscanf(msg.c_str(), "%*s %i#", &opt);
				if (ret != 1)
					std::cout << "Command x error" << std::endl;
				else
				{
					if (opt >= 0 && opt <= 180)
						servo.SetAngle(CHANNEL(1), ANGLE(opt));
				}
			}
			if (msg.find("y") != std::string::npos)
			{
				int opt;
				int ret = sscanf(msg.c_str(), "%*s %i#", &opt);
				if (ret != 1)
					std::cout << "Command y error" << std::endl;
				else
				{
					if (opt >= 0 && opt <= 180)
						servo.SetAngle(CHANNEL(0), ANGLE(opt));
				}
			}
			std::cout << msg << '\n';
		}
	}

	//while (true)
//{
//	digitalWrite(LED, HIGH);  // On
//	delay(500); // ms
//	digitalWrite(LED, LOW);	  // Off
//	delay(500);
//}

	return 0;
}


