#include "LaserServer.h"
#include <iostream>

LaserServer::LaserServer(unsigned port): ioService(), acceptor(ioService), connection(ioService)
{
	boost::asio::ip::tcp::endpoint endpoint(tcp::v4(), port);
	acceptor.open(endpoint.protocol());
	acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
	acceptor.bind(endpoint);
	acceptor.listen();

	std::cout << "Server: start \n";
	std::cout << "Server: listening on port " << port << '\n';
	startAcceptingConnection();

	ioThread = std::thread(&LaserServer::runService, this);
}

LaserServer::~LaserServer()
{
	ioService.stop();
}

const std::string LaserServer::popRequest()
{
	std::mutex m;
	if (requests.size() > 0)
	{
		auto req = requests.front();
		requests.pop();
		return req;
	}
	return std::string();
}

void LaserServer::startAcceptingConnection()
{
	acceptor.async_accept(connection, [this](const boost::system::error_code &ec) {
		handleAcceptConnection(ec);
	});
	std::cout << "Server: waiting for connection \n";
}

void LaserServer::handleAcceptConnection(const boost::system::error_code & ec)
{
	if (!ec)
	{
		std::cout << "Server: handle connection  \n";
		startReading();
	}
}

void LaserServer::startReading()
{
	boost::asio::async_read_until(connection, streambuf, '\n',
		[this](const boost::system::error_code e, std::size_t size) {
		readHandler(e, size);
	});
}

void LaserServer::readHandler(const boost::system::error_code e, std::size_t size)
{
	if (!e)
	{
		try
		{
			std::cout << "Server: recive message  \n";
			std::mutex m;
			std::istream is(&streambuf);
			std::string request;
			std::getline(is, request);

			requests.push(std::move(request));
		}
		catch (std::exception ex)
		{
			std::cerr << "ERROR: " << ex.what() << '\n';
		}
		startReading();
	}
	else if ((boost::asio::error::eof == e) || (boost::asio::error::connection_reset == e))
	{
		std::cout << "Server: client disconect \n";

		connection.close();
		startAcceptingConnection();
	}
	else
	{
		//obsluga bled�w
	}
}

void LaserServer::runService()
{
	try
	{
		ioService.run();
	}
	catch (...)
	{
	}
}
