#pragma once


#include <boost/asio.hpp>

#include <thread>
#include <mutex>
#include <set>
#include <queue>

using boost::asio::ip::tcp;

class LaserServer
{
public:
	LaserServer(unsigned port);
	~LaserServer();

	const std::string popRequest();

private:

	boost::asio::io_service ioService;
	std::thread ioThread;
	tcp::acceptor acceptor;
	tcp::socket connection;
	boost::asio::streambuf streambuf;

	std::queue<std::string > requests;

	void startAcceptingConnection();
	void handleAcceptConnection(const boost::system::error_code &ec);
	
	void startReading();
	void readHandler(const boost::system::error_code e, std::size_t size);

	void writeHandler(const boost::system::error_code& e, std::size_t size);

	void runService();
};

