#include <iostream>
#include <future>
#include <array>
#include <cmath>

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/bgsegm.hpp>

#ifndef CVUI_IMPLEMENTATION
	#define CVUI_IMPLEMENTATION
#endif
#include "cvui.h"	

#include "Camera.h"


// Convert to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

class bgsTest
{
public:
	bgsTest(cv::Ptr<cv::BackgroundSubtractor> substractor, const std::string& name);
	void apply(cv::InputArray in, float learningRate);

	const cv::Mat& getRgbMask() const;
	const cv::Mat& getFgMask() const;
private:
	const std::string name;
	cv::Ptr<cv::BackgroundSubtractor> substractor;
	cv::Mat fgMask, rgbFgMask;
};

constexpr auto WINDOW_NAME = "Live";

using namespace cv;

int main(int argc, char **argv)
{
	cv::CommandLineParser commandParser(argc, argv,
		"{help h||}"
		"{KNN knn||}"
		"{MOG mog||}"
		"{MOG2 mog2||}"
		"{GSOC gsoc||}"
		"{CNT cnt||}"
		"{GMG gmg||}"
		"{LSBP lsbp||}"
		"{a all||}"
		"{s save| ./videoTest.av | }"
	);

	if (!commandParser.check())
	{
		commandParser.printErrors();
		return 1;
	}

	//camera
	lt::Camera camera;
	int deviceID = 0;
	int apiID = cv::CAP_ANY;

	camera.open(deviceID, apiID);
	cv::Mat frame;
	if (!camera.isOpened()) {
		std::cerr << "ERROR! Unable to open camera\n";
		return -1;
	}

	//background substractors
	std::vector<bgsTest> substractors;

	if (commandParser.has("all"))
	{
		substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorGSOC(), "GSOC"));
		substractors.push_back(bgsTest(createBackgroundSubtractorKNN(),"KNN"));
		substractors.push_back(bgsTest(createBackgroundSubtractorMOG2(),"MOG2"));
		substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorMOG(),"MOG"));
		substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorGMG(),"GMG"));
		substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorLSBP(),"LSBP"));
		substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorCNT(), "CNT"));
	}
	else
	{
		if (commandParser.has("GSOC"))
			substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorGSOC(), "GSOC"));
		if (commandParser.has("KNN"))
			substractors.push_back(bgsTest(createBackgroundSubtractorKNN(), "KNN"));
		if (commandParser.has("MOG2"))
			substractors.push_back(bgsTest(createBackgroundSubtractorMOG2(), "MOG2"));
		if (commandParser.has("MOG"))
			substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorMOG(), "MOG"));
		if (commandParser.has("GMG"))
			substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorGMG(), "GMG"));
		if (commandParser.has("LSBP"))
			substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorLSBP(), "LSBP"));
		if (commandParser.has("CNT"))
			substractors.push_back(bgsTest(cv::bgsegm::createBackgroundSubtractorCNT(), "CNT"));
	}
	if (substractors.size() == 0)
	{
		std::cerr << "Select at least one substractor";
		return 2;
	}

	//window & screen
	const auto cW = camera.getWidth();
	const auto cH = camera.getHeigth();
	namedWindow(WINDOW_NAME, cv::WINDOW_NORMAL);
	cv::setWindowProperty(WINDOW_NAME, cv::WND_PROP_FULLSCREEN, cv::WINDOW_FULLSCREEN);
	auto matCount = substractors.size() + 1;
	matCount = static_cast<std::size_t>(std::ceil( std::sqrt(matCount)));
	cv::Mat screen(cv::Size(cW * matCount, cH * matCount), CV_8UC3);

	//settings window
	namedWindow("Settings", cv::WINDOW_NORMAL);
	cvui::init("Settings");
	cv::Mat settings(cv::Size(130, 100), CV_8UC3);
	auto autoLerning = true;
	int learning = 10;

	//video writer
	auto recording = false;
	cv::VideoWriter	videoWriter;
	if((recording=commandParser.has("save")))
	{
		const int codec = VideoWriter::fourcc('D', 'I', 'V', 'X');
		const double FRAME_RATE = 25;
		videoWriter.open(commandParser.get<cv::String>("save"), codec, FRAME_RATE, Size(screen.size().width, screen.size().height));
	}

	std::vector<std::future<void>> taskPool;

	for (;;)
	{
		double timer = (double)getTickCount();
		camera.read(frame);
		// check if we succeeded
		if (frame.empty()) {
			std::cerr << "ERROR! blank frame grabbed\n";
			break;
		}

		cvui::window(settings, 0, 0, cW, cH, "Settings");
		cvui::checkbox(settings, 15, 20, "auto learning", &autoLerning);
		cvui::trackbar(settings, 5, 50, 115, &learning, 0, 100);

		cvui::update();
		float learningRate;
		if (autoLerning)
			learningRate = -1;
		else
			learningRate = static_cast<float> (learning / 100);

		taskPool.clear();
		for (auto& substractor : substractors)
		{
			taskPool.push_back(std::async([&] {substractor.apply(frame, learningRate); }));
		}

		try
		{
			for (auto& task : taskPool)
			{
				task.get();
			}
		}
		catch(std::exception e)
		{
			std::cerr<<"exception "<< e.what() << "\n";
		}

		int y, x = y = 0;
		int w = int(cW);
		int h = int(cH);
		for (auto& substractor : substractors)
		{
			substractor.getRgbMask().copyTo(screen(cv::Rect(x, y, cW, cH)));

			x += int(cW);
			if (x >= screen.size().width)
			{
				y += int(cH);
				x = 0;
			}
		}
		frame.copyTo(screen(cv::Rect(x, y, cW, cH)));

		float fps = getTickFrequency() / ((double)getTickCount() - timer);
		cv::putText(screen, "FPS : " + SSTR(int(fps)), Point(300, 50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);
		cv::imshow(WINDOW_NAME, screen);
		cv::imshow("Settings", settings);

		if(recording)
			videoWriter.write(screen);

		if (waitKey(1) == 27) //esc
			return 0;


	}


	return 0;
}

bgsTest::bgsTest(cv::Ptr<cv::BackgroundSubtractor> substractor, const std::string & name): name(name), substractor(substractor)
{
}

void bgsTest::apply(cv::InputArray in, float learningRate)
{
	substractor->apply(in, fgMask, learningRate);
	cvtColor(fgMask, rgbFgMask, COLOR_GRAY2BGR);
	cv::putText(rgbFgMask, name, Point(100, 50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);
}

const cv::Mat & bgsTest::getRgbMask() const
{
	return rgbFgMask;
}

const cv::Mat & bgsTest::getFgMask() const
{
	return fgMask;
}
