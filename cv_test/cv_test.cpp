#include <iostream>

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>
#include <opencv2/video.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/bgsegm.hpp>
#include <future>
#include "Camera.h"
#include "cv_test.h"	

#ifndef CVUI_IMPLEMENTATION
	#define CVUI_IMPLEMENTATION
#endif
#include "cvui.h"	

#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/video/tracking.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"

#include <vector>
#include <stdio.h>
#include <iostream>


using namespace cv;
using namespace std;





using namespace cv;
using namespace std;

int main(int argc, char** argv)
{
	return denseOptFlow(argc, argv);
//	return opticalFlow(argc, argv);
//	//return tracking(argc,argv);
//	//return backgroundSubstraction();
//	//return cascadeFaceDetector(argc, argv);
}

// Convert to string
#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

constexpr auto WINDOW_NAME = "Live";

int opticalFlow(int argc, char** argv)
{
	Mat frame;
	Mat modifyFrame;
	Mat prevFrame;
	Mat flow;
	//VideoCapture camera;
	//lt::Camera camera;

	VideoCapture camera("Street - 5025.mp4");

	int deviceID = 0;
	int apiID = cv::CAP_ANY;

	camera.open(deviceID, apiID);
	//camera.open("D:\Code\Szkola\Projekt_inz\Street - 5025.mp4");

	 //check if we succeeded
	if (!camera.isOpened()) {
		std::cerr << "ERROR! Unable to open camera\n";
		return -1;
	}
	std::cout << "Start grabbing" << std::endl
		<< "Press any key to terminate" << std::endl;

	namedWindow(WINDOW_NAME, 1);

	Mat fm;
	for (;;)
	{
		double timer = (double)getTickCount();
		// wait for a new frame from camera and store it into 'frame'
		camera.read(frame);

		
		//cv::GaussianBlur(frame, frame, Size(15, 15), 3);
			
			
		// check if we succeeded
		if (frame.empty()) {
			std::cerr << "ERROR! blank frame grabbed\n";
			break;
		}
		cvtColor(frame, modifyFrame, CV_BGR2GRAY);

		if (prevFrame.empty() == false)
		{

			// calculate optical flow 
			calcOpticalFlowFarneback(prevFrame, modifyFrame, flow, 0.4, 1, 12, 2, 8, 1.2, 0);


			for (int y = 0; y < frame.rows; y += 25)
			{
				for (int x = 0; x < frame.cols; x += 25)
				{
					// get the flow from y, x position * 10 for better visibility
					const Point2f flowatxy = flow.at<Point2f>(y, x) * 10;
					// draw line at flow direction
					line(frame, Point(x, y), Point(cvRound(x + flowatxy.x), cvRound(y + flowatxy.y)), Scalar(255, 0, 0));
					// draw initial point
					circle(frame, Point(x, y), 1, Scalar(0, 0, 0), -1);

				}
			}
		}
		else 
		{
			// fill previous image in case prevgray.empty() == true
			modifyFrame.copyTo(prevFrame);
		}

		// Calculate Frames per second (FPS)
		float fps = getTickFrequency() / ((double)getTickCount() - timer);


		// Display FPS on frame
		cv::putText(frame, "FPS : " + SSTR(int(fps)), Point(100, 50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);

		// Display frame.
		cv::imshow(WINDOW_NAME, frame);
		if (waitKey(1) >= 0)
			return 0;
	}

	return 0;
}

int tracking(int argc, char** argv)
{
	auto trackerChoise = 0;
	if (argc > 1)
	{
		try 
		{
			trackerChoise = std::stoi(argv[1]);
		}
		catch (std::exception e)
		{
			std::cerr << e.what();
			return 1;
		}
	}
    //create a gui window:
    

    Mat frame;
    
	//VideoCapture camera;
	lt::Camera camera;
	int deviceID = 0;             
	int apiID = cv::CAP_ANY;

	camera.open(deviceID , apiID);
	// check if we succeeded
	if (!camera.isOpened()) {
		std::cerr << "ERROR! Unable to open camera\n";
		return -1;
	}
    
	const auto cW = camera.getWidth();
	const auto cH = camera.getHeigth();

	std::cout << cW << "x" << cH << '\n';

	

	// List of tracker types in OpenCV 3.4.1
	std::string trackerTypes[8] = { "BOOSTING", "MIL", "KCF", "TLD","MEDIANFLOW", "GOTURN", "MOSSE", "CSRT" };
	// vector <string> trackerTypes(types, std::end(types));

	// Create a tracker
	string trackerType = trackerTypes[trackerChoise];

	Ptr <Tracker > tracker;

	if (trackerType == "BOOSTING")
		tracker = TrackerBoosting::create();
	if (trackerType == "MIL")
		tracker = TrackerMIL::create();
	if (trackerType == "KCF")
	{
		TrackerKCF::Params param;
		param.desc_pca = TrackerKCF::CN; // TrackerKCF::GRAY |
		tracker = TrackerKCF::create(param);
	}
		
	if (trackerType == "TLD")
		tracker = TrackerTLD::create();
	if (trackerType == "MEDIANFLOW")
		tracker = TrackerMedianFlow::create();
	if (trackerType == "GOTURN")
		tracker = TrackerGOTURN::create();
	if (trackerType == "MOSSE")
		tracker = TrackerMOSSE::create();
	if (trackerType == "CSRT")
		tracker = TrackerCSRT::create();
	
	// Read first frame 
	camera.read(frame);
	
	// Define initial bounding box 
	Rect2d bbox(287, 23, 86, 320);

	namedWindow(WINDOW_NAME, 1);
	// Uncomment the line below to select a different bounding box 
	bbox = selectROI(WINDOW_NAME,frame, false); 
	// Display bounding box. 
	rectangle(frame, bbox, Scalar(255, 0, 0), 2, 1);
	Mat modifyFrame;
	cvtColor(frame, modifyFrame, CV_BGR2HSV);

	tracker->init(modifyFrame, bbox);

	namedWindow(WINDOW_NAME, 1);
	resizeWindow(WINDOW_NAME, cW * 2, cH * 2);
	Mat windowScreen(Size(cW * 2, cH * 2), CV_8UC3);

	frame.copyTo(windowScreen(cv::Rect(0, 0, cW, cH)));
	modifyFrame.copyTo(windowScreen(cv::Rect(cW, cH, cW, cH)));

	std::cout << "Start grabbing" << std::endl
		<< "Press any key to terminate" << std::endl;

	for (;;)
	{
		// wait for a new frame from camera and store it into 'frame'
		camera.read(frame);
		// check if we succeeded
		if (frame.empty()) {
			std::cerr << "ERROR! blank frame grabbed\n";
			break;
		}
		cvtColor(frame, modifyFrame, CV_BGR2HSV);

			// Start timer
		double timer = (double)getTickCount();

		// Update the tracking result
		//bool ok = tracker->update(frame, bbox);
		bool ok = tracker->update(modifyFrame, bbox);

		// Calculate Frames per second (FPS)
		float fps = getTickFrequency() / ((double)getTickCount() - timer);

		if (ok)
		{
			// Tracking success : Draw the tracked object
			rectangle(frame, bbox, Scalar(255, 0, 0), 2, 1);
		}
		else
		{
			// Tracking failure detected.
			cv::putText(frame, "Tracking failure detected", Point(100, 80), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(0, 0, 255), 2);
		}

		// Display tracker type on frame
		cv::putText(frame, trackerType + " Tracker", Point(100, 20), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);

		// Display FPS on frame
		cv::putText(frame, "FPS : " + SSTR(int(fps)), Point(100, 50), FONT_HERSHEY_SIMPLEX, 0.75, Scalar(50, 170, 50), 2);

		// Display frame.
		frame.copyTo(windowScreen(cv::Rect(0, 0, cW, cH)));
		modifyFrame.copyTo(windowScreen(cv::Rect(cW, cH, cW, cH)));
		cv::imshow(WINDOW_NAME, windowScreen);
		if (waitKey(1) >= 0)
			return 0;
	}
	return 0; 
}

void detectAndDisplay(Mat frame);
CascadeClassifier face_cascade;
CascadeClassifier eyes_cascade;
CascadeClassifier cat_cascade;
int cascadeFaceDetector(int argc,  char** argv)
{
	CommandLineParser parser(argc, argv,
		"{help h||}"
		"{face_cascade|../../data/haarcascades/haarcascade_frontalface_alt.xml|Path to face cascade.}"
		"{eyes_cascade|../../data/haarcascades/haarcascade_eye_tree_eyeglasses.xml|Path to eyes cascade.}"
		"{camera|0|Camera device number.}");
	parser.about("\nThis program demonstrates using the cv::CascadeClassifier class to detect objects (Face + eyes) in a video stream.\n"
		"You can use Haar or LBP features.\n\n");
	parser.printMessage();
	String face_cascade_name = parser.get<String>("face_cascade");
	String eyes_cascade_name = parser.get<String>("eyes_cascade");
	String cat_cascade_name = "../../data/catClassifier/cascade.xml";
	//-- 1. Load the cascades
	//if (!face_cascade.load(face_cascade_name))
	//{
	//	cout << "--(!)Error loading face cascade\n";
	//	return -1;
	//};
	//if (!eyes_cascade.load(eyes_cascade_name))
	//{
	//	cout << "--(!)Error loading eyes cascade\n";
	//	return -1;
	//};
	if (!cat_cascade.load(cat_cascade_name))
	{
		cout << "--(!)Error loading cat cascade\n";
		return -1;
	};
	//int camera_device = parser.get<int>("camera");
	VideoCapture capture;
	//-- 2. Read the video stream
	capture.open(0);
	if (!capture.isOpened())
	{
		cout << "--(!)Error opening video capture\n";
		return -2;
	}
	Mat frame;
	while (capture.read(frame))
	{
		if (frame.empty())
		{
			cout << "--(!) No captured frame -- Break!\n";
			break;
		}
		//-- 3. Apply the classifier to the frame
		detectAndDisplay(frame);
		if (waitKey(10) == 27)
		{
			break; // escape
		}
	}
	return 0;
}
void detectAndDisplay(Mat frame)
{
	Mat frame_gray;
	cvtColor(frame, frame_gray, COLOR_BGR2GRAY);
	equalizeHist(frame_gray, frame_gray);
	//-- Detect faces
	std::vector<Rect> faces;
	//face_cascade.detectMultiScale(frame_gray, faces);
	cat_cascade.detectMultiScale(frame_gray, faces);
	for (size_t i = 0; i < faces.size(); i++)
	{
		Point center(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height / 2);
		ellipse(frame, center, Size(faces[i].width / 2, faces[i].height / 2), 0, 0, 360, Scalar(255, 0, 255), 4);
		Mat faceROI = frame_gray(faces[i]);
		//-- In each face, detect eyes
		//std::vector<Rect> eyes;
		//eyes_cascade.detectMultiScale(faceROI, eyes);
		/*for (size_t j = 0; j < eyes.size(); j++)
		{
			Point eye_center(faces[i].x + eyes[j].x + eyes[j].width / 2, faces[i].y + eyes[j].y + eyes[j].height / 2);
			int radius = cvRound((eyes[j].width + eyes[j].height)*0.25);
			circle(frame, eye_center, radius, Scalar(255, 0, 0), 4);
		}*/
	}
	//-- Show what you got
	cv::imshow("Capture - Face detection", frame);
}


int denseOptFlow(int argc,  char** argv)
{

	// add your file name
	//VideoCapture cap("Street - 5025.mp4");
	int deviceID = 0;
	int apiID = cv::CAP_ANY;

	VideoCapture cap;
	cap.open(deviceID, apiID);

	Mat flow, frame;
	// some faster than mat image container
	UMat  flowUmat, prevgray;
	Mat flowScreen;
	Mat features;
	for (;;)
	{

		bool Is = cap.grab();
		if (Is == false) {
			// if video capture failed
			cout << "Video Capture Fail" << endl;
			break;
		}
		else {



			Mat img;
			Mat original;

			// capture frame from video file
			cap.retrieve(img, cv::CAP_OPENNI_BGR_IMAGE);
			resize(img, img, Size(640, 480));
			//cv::GaussianBlur(img, img, Size(33, 33), 3, 3);
			//cv::medianBlur(img, img, 31);


			// save original for later
			img.copyTo(original);

			// just make current frame gray
			cvtColor(img, img, COLOR_BGR2GRAY);
			cv::medianBlur(img, img, 21);

			//cv::adaptiveThreshold(img, img, 255, cv::ADAPTIVE_THRESH_MEAN_C, cv::THRESH_BINARY, 11, 2);
			//cv::threshold(img, img, 0, 255, cv::THRESH_BINARY | cv::THRESH_OTSU);
			imshow("blur", img);
			

			if (prevgray.empty() == false) {

				// calculate optical flow 
				calcOpticalFlowFarneback(prevgray, img, flowUmat, 0.5, 3, 5, 3, 5, 1.1, 0);
				// copy Umat container to standard Mat
				flowUmat.copyTo(flow);

				cv::Mat hsv;
				// By y += 5, x += 5 you can specify the grid 
				for (int y = 0; y < original.rows; y += 5) {
					for (int x = 0; x < original.cols; x += 5)
					{
						// get the flow from y, x position * 10 for better visibility
						const Point2f flowatxy = flow.at<Point2f>(y, x);// *10;
						// draw line at flow direction
						//line(original, Point(x, y), Point(cvRound(x + flowatxy.x), cvRound(y + flowatxy.y)), Scalar(255, 0, 0));
						// draw initial point
						//circle(original, Point(x, y), 1, Scalar(0, 0, 0), -1);


					}

				}

				cv::Mat xy[2]; //X,Y
				cv::split(flow, xy);
				cv::Mat magnitude, angle;
				cv::cartToPolar(xy[0], xy[1], magnitude, angle, true);

				
				//translate magnitude to range [0;1]
				double mag_max;
				cv::minMaxLoc(xy[0], 0, &mag_max);
				std::cout << "max magnitude before conversion: " << mag_max << '\n';
				cv::minMaxLoc(magnitude, 0, &mag_max);
				//magnitude.convertTo(magnitude, -1, 1.0 / mag_max);

				std::cout << "max magnitude: " << mag_max << '\n';

				cv::Mat _hsv[3];
				_hsv[0] = cv::Mat::ones(angle.size(), CV_32F);// angle;
				_hsv[1] = cv::Mat::ones(angle.size(), CV_32F);
				_hsv[2] = magnitude;
				cv::merge(_hsv, 3, hsv);

				cv::Mat bin;
				cv::inRange(magnitude, cv::Scalar(7,0), cv::Scalar(255.0f,255), bin);

				cv::dilate(bin, bin, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(9, 9)), cv::Point(-1, -1), 8);
				imshow("bin", bin);
				//for(auto m : magnitude)




				// draw the results
				namedWindow("prew", WINDOW_AUTOSIZE);
				imshow("prew", original);


				cvtColor(hsv, hsv, COLOR_HSV2BGR);
				namedWindow("flow", WINDOW_AUTOSIZE);
				imshow("flow", hsv);


				// fill previous image again
				img.copyTo(prevgray);

			}
			else {

				// fill previous image in case prevgray.empty() == true
				img.copyTo(prevgray);
			}


			int key1 = waitKey(20);

		}
	}
	return 0;
}

//static void onMouse(int event, int x, int y, int /*flags*/, void* /*param*/)
//{
//	if (event == EVENT_LBUTTONDOWN)
//	{
//		point = Point2f((float)x, (float)y);
//		addRemovePt = true;
//	}
//}
Point2f point;
bool addRemovePt = false;
//{
	//VideoCapture cap;
	//TermCriteria termcrit(TermCriteria::COUNT | TermCriteria::EPS, 20, 0.03);
	//Size subPixWinSize(10, 10), winSize(31, 31);
	//const int MAX_COUNT = 500;
	//bool needToInit = false;
	//bool nightMode = false;
	//cv::CommandLineParser parser(argc, argv, "{@input|0|}");
	//string input = parser.get<string>("@input");
	//if (input.size() == 1 && isdigit(input[0]))
	//	cap.open(input[0] - '0');
	//else
	//	cap.open(input);
	//if (!cap.isOpened())
	//{
	//	cout << "Could not initialize capturing...\n";
	//	return 0;
	//}
	//namedWindow("LK Demo", 1);
	//setMouseCallback("LK Demo", onMouse, 0);
	//Mat gray, prevGray, image, frame;
	//vector<Point2f> points[2];
	//for (;;)
	//{
	//	cap >> frame;
	//	if (frame.empty())
	//		break;
	//	frame.copyTo(image);
	//	cvtColor(image, gray, COLOR_BGR2GRAY);
	//	if (nightMode)
	//		image = Scalar::all(0);
	//	if (needToInit)
	//	{
	//		// automatic initialization
	//		goodFeaturesToTrack(gray, points[1], MAX_COUNT, 0.01, 10, Mat(), 3, 0, 0.04);
	//		cornerSubPix(gray, points[1], subPixWinSize, Size(-1, -1), termcrit);
	//		addRemovePt = false;
	//	}
	//	else if (!points[0].empty())
	//	{
	//		vector<uchar> status;
	//		vector<float> err;
	//		if (prevGray.empty())
	//			gray.copyTo(prevGray);
	//		calcOpticalFlowPyrLK(prevGray, gray, points[0], points[1], status, err, winSize,
	//			3, termcrit, 0, 0.001);
	//		size_t i, k;
	//		for (i = k = 0; i < points[1].size(); i++)
	//		{
	//			if (addRemovePt)
	//			{
	//				if (norm(point - points[1][i]) <= 5)
	//				{
	//					addRemovePt = false;
	//					continue;
	//				}
	//			}
	//			if (!status[i])
	//				continue;
	//			points[1][k++] = points[1][i];
	//			circle(image, points[1][i], 3, Scalar(0, 255, 0), -1, 8);
	//		}
	//		points[1].resize(k);
	//	}
	//	if (addRemovePt && points[1].size() < (size_t)MAX_COUNT)
	//	{
	//		vector<Point2f> tmp;
	//		tmp.push_back(point);
	//		cornerSubPix(gray, tmp, winSize, Size(-1, -1), termcrit);
	//		points[1].push_back(tmp[0]);
	//		addRemovePt = false;
	//	}
	//	needToInit = false;
	//	imshow("LK Demo", image);
	//	char c = (char)waitKey(10);
	//	if (c == 27)
	//		break;
	//	switch (c)
	//	{
	//	case 'r':
	//		needToInit = true;
	//		break;
	//	case 'c':
	//		points[0].clear();
	//		points[1].clear();
	//		break;
	//	case 'n':
	//		nightMode = !nightMode;
	//		break;
	//	}
	//	std::swap(points[1], points[0]);
	//	cv::swap(prevGray, gray);
	//}
	//return 0;
//}