#pragma once
#include <opencv2/opencv.hpp>

//tracking tutorial
int tracking(int argc, char** argv);

// cascade classifier tut
int cascadeFaceDetector(int argc, char** argv);
void detectAndDisplay(cv::Mat frame);
int opticalFlow(int argc, char** argv);
int denseOptFlow(int argc,  char** argv);
//int sparseOptFlow(int argc,  char** argv);