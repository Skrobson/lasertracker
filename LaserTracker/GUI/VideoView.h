#pragma once

#include "VideoScreen.h"

#include <QWidget>
#include <QLabel>
#include <QPushButton>

#include <opencv2/opencv.hpp>
#include <memory>

class VideoView : public QWidget
{
    Q_OBJECT

public:
    explicit VideoView(QWidget *parent = nullptr);
    ~VideoView();

	void showImage(cv::InputArray image);

signals:
	void rectSelected(cv::Rect);

private:
	VideoScreen* videoScreen;
	QPushButton * recButton;

};


