#include "MainComponentTab.h"

#include <QHBoxLayout>
#include <QLabel>
#include <QDebug>

MainComponentTab::MainComponentTab(QWidget* parent):QWidget(parent) 
{
	methodsBox = new QComboBox(this);
	mainLayout = new QVBoxLayout(this);

	connect(methodsBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
		[=](int idx) {changeView(idx); });

	startButton = new QPushButton(tr("start"),this);

	connect(startButton, &QPushButton::clicked, this, [this](bool) {
		if (currentIndex)
			emit algorithmChanged(currentIndex);
		else
			emit noneAlgorithm();
	});

	//method choose layout
	QHBoxLayout * methodsLayout = new QHBoxLayout();
	QLabel* methodsLabel = new QLabel(this);
	methodsLabel->setText(tr("Methods: "));
	methodsLayout->addWidget(methodsLabel);
	methodsLayout->addWidget(methodsBox);
	methodsLayout->addWidget(startButton);

	stackedWidget = new QStackedWidget(this);
	QWidget* emptyWidget{ new QWidget(this) };

	algorithms.push_back(std::make_pair(tr("None"), emptyWidget));
	stackedWidget->addWidget(emptyWidget);
	methodsBox->addItem(algorithms[0].first);

	//main layout
	mainLayout->addLayout(methodsLayout);
	mainLayout->addWidget(stackedWidget);
	
	
}


int MainComponentTab::addAlgorithmView(QWidget * view, const QString & name)
{
	algorithms.push_back(std::make_pair(name, view));
	methodsBox->addItem(name);
	return stackedWidget->addWidget(view);
}

void MainComponentTab::changeView(int index)
{
	qDebug() << "idx: " << index << "\r\n";
	stackedWidget->setCurrentIndex(index);
	currentIndex = index;
}
