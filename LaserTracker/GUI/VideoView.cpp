#include "VideoView.h"
#include <QHBoxLayout>
VideoView::VideoView(QWidget *parent) :
    QWidget(parent)
{
	videoScreen = new VideoScreen(this);
	recButton = new QPushButton(this);
	recButton->setText("REC");

	QHBoxLayout* videoLayout{ new QHBoxLayout(this) };
	videoLayout->addWidget(recButton);
	videoLayout->addWidget(videoScreen);

	connect(videoScreen, &VideoScreen::rectSelected, this, [this](cv::Rect boundingBox) {
		emit rectSelected(boundingBox);
	});
}

VideoView::~VideoView()
{
}

void VideoView::showImage(cv::InputArray image)
{
	videoScreen->showImage(image);
}
