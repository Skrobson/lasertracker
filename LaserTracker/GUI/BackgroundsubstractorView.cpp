#include "BackgroundsubstractorView.h"

#include <QHBoxLayout>
#include <QLabel>
BackgroundsubstractorView::BackgroundsubstractorView(QWidget * parent) :QWidget(parent) 
{
	autolearningCheckBox = new QCheckBox(tr("Auto learning"), this);
	autolearningCheckBox->setChecked(true);

	learningRateSpinBox = new QDoubleSpinBox(this);
	learningRateSpinBox->setMaximum(1.0);
	learningRateSpinBox->setMinimum(0.0);
	learningRateSpinBox->setDecimals(4);
	learningRateSpinBox->setSingleStep(0.001);
	QLabel* learningRateLabel{ new QLabel(tr("Learning rate"),this) };

	QHBoxLayout* spinBoxLayout{ new QHBoxLayout };
	spinBoxLayout->addWidget(learningRateLabel);
	spinBoxLayout->addWidget(learningRateSpinBox);

	mainLayout = new QVBoxLayout(this);
	mainLayout->addWidget(autolearningCheckBox);
	mainLayout->addLayout(spinBoxLayout);

	connect(autolearningCheckBox, &QCheckBox::stateChanged,this, [=](int state) {
		if (state)
			emit learningRateChanged(-1);
		else
			emit learningRateChanged(learningRateSpinBox->value());
	});

	connect(learningRateSpinBox, QOverload<double>::of(&QDoubleSpinBox::valueChanged), this,
		[=](double d) {
		if(!autolearningCheckBox->isChecked())
			emit learningRateChanged(d); 
	});
}
