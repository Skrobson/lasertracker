#pragma once
#include "MainComponentTab.h"

#include <QObject>


class DetectorTab : public MainComponentTab
{
	Q_OBJECT
public:
	DetectorTab(QWidget* parent = nullptr);

signals:
	void detectionAlgorithmChanged();

protected:
	
};