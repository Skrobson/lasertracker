#include "VideoScreen.h"	
#include <QDebug>
VideoScreen::VideoScreen(QWidget * parent):QLabel(parent) 
{
	setCursor(QCursor(Qt::CursorShape::CrossCursor));
	setFixedWidth(WIDTH);
	setFixedHeight(HEIGHT);
	setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
	setStyleSheet("QLabel{border-radius: 0px; background: black;}");
}

void VideoScreen::showImage(cv::InputArray image)
{
	cv::Mat screenImg;
	cv::cvtColor(image, screenImg, cv::COLOR_BGR2RGB);
	if (rMouseButtonPressed)
		cv::rectangle(screenImg, selector, color);
	//cv::resize(screenImg, screenImg, cv::Size(WIDTH, HEIGHT));
	auto pixmap = QPixmap::fromImage(QImage(screenImg.data, screenImg.cols, screenImg.rows, screenImg.step, QImage::Format_RGB888));
	//pixmap.scaled(QSize(WIDTH, HEIGHT));
	setPixmap(pixmap);
}

void VideoScreen::mousePressEvent(QMouseEvent * ev)
{
	if (ev->button() == Qt::MouseButton::RightButton && !rMouseButtonPressed)
	{
		rMouseButtonPressed = true;
		startPoint = cv::Point{ ev->x() ,ev->y() };
		selector.x = ev->x();
		selector.y = ev->y();
		selector.width = 0;
		selector.height = 0;


		QString x = QString::number(ev->x());
		QString y = QString::number(ev->y());
		qDebug() << x << "," << y;
	}
	if (ev->button() == Qt::MouseButton::LeftButton && rMouseButtonPressed)
	{
		rMouseButtonPressed = false;
		endPoint = cv::Point{ ev->x() ,ev->y() };

		rectSelected(selector);

		QString x = QString::number(ev->x());
		QString y = QString::number(ev->y());
		qDebug() << x << "," << y;
		
	}
}

void VideoScreen::mouseMoveEvent(QMouseEvent * ev)
{
	if (rMouseButtonPressed)
	{
		selector.width = (ev->x() - selector.x);
		selector.height =  (ev->y() - selector.y);
	}
}

void VideoScreen::mouseReleaseEvent(QMouseEvent * ev)
{
	if (ev->button() == Qt::MouseButton::RightButton)
		rMouseButtonPressed = false;
}
