#pragma once
#include "MainComponentTab.h"

class TrackerTab : public MainComponentTab
{
public:
	TrackerTab(QWidget* parent = nullptr);


signals:
	void trackingAlgorithChanged();
};