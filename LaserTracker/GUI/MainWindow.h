#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QTabWidget>
#include <QStatusBar>
#include <opencv2/opencv.hpp>

#include <memory>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
	void showImage(cv::InputArray image);
private:

	QWidget central;
};

#endif // MAINWINDOW_H
