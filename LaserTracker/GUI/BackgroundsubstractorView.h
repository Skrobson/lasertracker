#pragma once
#include <QWidget>
#include <QDoubleSpinBox>
#include <QCheckBox>
#include <QVBoxLayout>

class BackgroundsubstractorView : public QWidget
{
	Q_OBJECT
public:

	BackgroundsubstractorView(QWidget* parent = nullptr);

protected:
	QDoubleSpinBox* learningRateSpinBox;
	QCheckBox* autolearningCheckBox;
	QVBoxLayout* mainLayout;
signals:
	void learningRateChanged(double val);
};