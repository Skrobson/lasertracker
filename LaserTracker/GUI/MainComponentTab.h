#pragma once

#include <QWidget>
#include <QVBoxLayout>
#include <QComboBox>
#include <QPushButton>
#include <QString>
#include <QStackedWidget>

#include <vector>
#include <tuple>

class MainComponentTab : public QWidget
{
	Q_OBJECT
public:
	MainComponentTab(QWidget * parent = nullptr);
	
	int addAlgorithmView(QWidget* view, const QString& name);

signals:
	void algorithmChanged(int index);
	void noneAlgorithm();
protected:
	int currentIndex = 0;

	QComboBox* methodsBox;
	QVBoxLayout* mainLayout;
	QPushButton* startButton;
	void changeView(int index);

	std::vector<std::pair<QString , QWidget*>> algorithms;

private:
	QStackedWidget* stackedWidget;
};