#pragma once

#include <QLabel>
#include <QMouseEvent>
#include <QPainter>

#include <opencv2/opencv.hpp>


class VideoScreen : public QLabel
{
	Q_OBJECT
public:
	VideoScreen(QWidget* parent = nullptr);
	void showImage(cv::InputArray image);

	static const int WIDTH = 640; //800
	static const int HEIGHT = 480; //600

	void mousePressEvent(QMouseEvent* ev) override;
	void mouseMoveEvent(QMouseEvent *ev) override;
	void mouseReleaseEvent(QMouseEvent *ev) override;

signals:
	void rectSelected(cv::Rect);

private:
	bool rMouseButtonPressed = false;
	cv::Point startPoint;
	cv::Point endPoint;
	cv::Rect selector;
	cv::Scalar color{ 255,0,0 };
};