#include "Camera.h"

using namespace lt;
int Camera::getHeigth() const
{
	const auto h= this->get(cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT);
	return static_cast<int>(h);
}

int Camera::getWidth() const
{
	const auto w = this->get(cv::VideoCaptureProperties::CAP_PROP_FRAME_WIDTH);
	return static_cast<int>(w);
}

double Camera::getFramerate() const
{
	const auto framerate = this->get(cv::VideoCaptureProperties::CAP_PROP_FRAME_HEIGHT);
	return framerate;
}
