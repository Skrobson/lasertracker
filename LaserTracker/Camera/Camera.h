#pragma once
#include <opencv2/opencv.hpp>

namespace lt
{
	class Camera : public cv::VideoCapture
	{
	public:
		int getHeigth() const;
		int getWidth() const;
		double getFramerate() const;
	};
}
