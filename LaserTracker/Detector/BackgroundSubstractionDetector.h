#pragma once
#include "Detector.h"

namespace lt { namespace internal
{
	class BackgroundSubstractionDetector : public Detector
	{
	public:
		virtual std::vector<cv::Rect> detect(cv::InputArray	frame) override = 0;

		const cv::Mat& getForegroundMask() const;
		const cv::Mat& getMorphologyMat() const;


		void setLearningRate(double rate);
	protected:
		double learningRate = -1;
		cv::Mat foregroundMask;
		cv::Mat afterMorphology;

		const cv::Mat MORPH_KERNEL = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(5, 5));
		//	<< 
		//	1, 1, 1, 1, 1,
		//	1, 1, 1, 1, 1,
		//	1, 1, 1, 1, 1,
		//	1, 1, 1, 1, 1,
		//	1, 1, 1, 1, 1);

	};
	}
}