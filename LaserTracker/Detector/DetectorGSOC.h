#pragma once
#include "Detector.h"
#include "BackgroundSubstractionDetector.h"

#include <opencv2/opencv.hpp>
#
#include <opencv2/bgsegm.hpp>

namespace lt
{
	class DetectorGSOC : public internal::BackgroundSubstractionDetector
	{
	public:
		DetectorGSOC();

		std::vector<cv::Rect> detect(cv::InputArray frame) override;

	private:

		//void removeShadows();
		cv::Ptr<cv::bgsegm::BackgroundSubtractorGSOC> substractor;

	};
}