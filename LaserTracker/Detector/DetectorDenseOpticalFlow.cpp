#include "DetectorDenseOpticalFlow.h"

std::vector<cv::Rect> lt::DenseOpticalFlowDetector::detect(cv::InputArray frame)
{
	cv::cvtColor(frame, currentFrame, cv::COLOR_BGR2GRAY);
	cv::medianBlur(currentFrame, currentFrame, 21);

	if (prevFrame.empty())
	{
		currentFrame.copyTo(prevFrame);
		return std::vector<cv::Rect>();
	}
	

	calcOpticalFlowFarneback(prevFrame, currentFrame, flowUmat, 0.5, 3, 5, 3, 5, 1.1, 0);
	flowUmat.copyTo(flow);

	cv::Mat xy[2]; //X,Y
	cv::split(flow, xy);
	cv::Mat magnitude, angle;
	cv::cartToPolar(xy[0], xy[1], magnitude, angle, true);

	cv::Mat bin;
	cv::inRange(magnitude, cv::Scalar(7, 0), cv::Scalar(255.0f, 255), bin);

	cv::dilate(bin, bin, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(9, 9)), cv::Point(-1, -1), 8);

	return findContours(bin);
}
