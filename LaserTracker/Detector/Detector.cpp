#include "Detector.h"

std::vector<cv::Rect> lt::Detector::findContours(const cv::Mat& src)
{
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Vec4i> hierarchy;

	cv::findContours(src, contours, hierarchy, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE);

	std::vector<cv::Rect> boundingBoxes;


	for (auto& con : contours)
	{
		//cv::RotatedRect boundingBox = cv::minAreaRect(con);
		//cv::Point2f corners[4]= cv::boundingRect(con);
		cv::Rect boundingBox = cv::boundingRect(con);

		//boundingBox.points(corners);
		boundingBoxes.push_back(std::move(std::ref(boundingBox)));
	}
	return boundingBoxes;
}