#include "BackgroundSubstractionDetector.h"
#include "DetectorGSOC.h"

lt::DetectorGSOC::DetectorGSOC(): substractor(cv::bgsegm::createBackgroundSubtractorGSOC())
{
}

std::vector<cv::Rect> lt::DetectorGSOC::detect(cv::InputArray frame)
{
	substractor->apply(frame, foregroundMask, learningRate);

	//cv::erode(foregroundMask, afterMorphology, MORPH_KERNEL,cv::Point(-1, -1), 5);
	cv::morphologyEx(foregroundMask, afterMorphology, cv::MORPH_OPEN, MORPH_KERNEL, cv::Point(-1, -1), 2);
	cv::morphologyEx(afterMorphology, afterMorphology, cv::MORPH_CLOSE, MORPH_KERNEL, cv::Point(-1, -1), 3);



	return findContours(afterMorphology);
}
