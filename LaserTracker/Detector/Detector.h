#pragma once

#include <vector>

#include <opencv2/opencv.hpp>


namespace lt
{
	class Detector
	{
	public:

		virtual std::vector<cv::Rect> detect(cv::InputArray) = 0;

	protected:
		std::vector<cv::Rect> detectetObjects;
		virtual std::vector<cv::Rect> findContours(const cv::Mat& src);

	};
}