#include "BackgroundSubstractionDetector.h"

const cv::Mat& lt::internal::BackgroundSubstractionDetector::getForegroundMask() const
{
	return foregroundMask;
}

const cv::Mat& lt::internal::BackgroundSubstractionDetector::getMorphologyMat() const
{
	return afterMorphology;
}

void lt::internal::BackgroundSubstractionDetector::setLearningRate(double rate)
{
	learningRate = rate; //check what happen when value is outwith scope
}


