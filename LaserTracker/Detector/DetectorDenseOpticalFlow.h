#pragma once
#include "Detector.h"

#include <opencv2/opencv.hpp>
#include "opencv2/imgproc.hpp"
#include "opencv2/objdetect/objdetect.hpp"

namespace lt
{
	class DenseOpticalFlowDetector : public lt::Detector
	{
	public:
		std::vector<cv::Rect> detect(cv::InputArray);

	private:
		cv::Mat flow, currentFrame;
		// some faster than mat image container
		cv::UMat  flowUmat, prevFrame;
	};
}
