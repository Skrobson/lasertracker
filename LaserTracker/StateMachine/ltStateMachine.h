#pragma once
#include "Detector.h"

#include <opencv2/tracking.hpp>

#include <memory>	
#include <functional>
#include <atomic>
#include <mutex>

namespace lt
{
	enum class ltPhase
	{
		IDLE,
		DETECTION,
		TRACKING
	};

	using phaseTransitionCallback = std::function<void(lt::ltPhase phase)>;

	class ltStateMachine
	{
	public:
		ltStateMachine();

		void setDetector(std::shared_ptr<lt::Detector> detector);		
		void setTracker(std::function<cv::Ptr<cv::Tracker>()>);

		void startIdle();
		void startDetection();
		void startTracking(cv::InputArray frame, const cv::Rect2d& boundingBox);
		void startPreviousState();

		//w zaleznosci od stanu zwraca wykryte obiekty, sledzony obiekt, pusty vector
		std::vector<cv::Rect> step(cv::InputArray frame);

		void setPhaseTransitionCallback(phaseTransitionCallback&& callback);
		
	private:
		std::atomic<ltPhase> phase;  
		std::atomic<ltPhase> previousPhase; 

		phaseTransitionCallback callback;

		std::function<cv::Ptr<cv::Tracker>()> trackerFactoryMethod;
		std::shared_ptr<Detector> detector = nullptr;
		cv::Ptr<cv::Tracker> tracker;

		std::vector<cv::Rect> trackerPhase(cv::InputArray frame);

		std::mutex m;

		cv::Rect2d previousTrackedObject; 

		//not accurate distance
		//if -1 there was not valid rect
		int calcDistance(const cv::Rect&, const cv::Rect&);
	};
}

