#include "ltStateMachine.h"

#include <future>
#include <cmath>

lt::ltStateMachine::ltStateMachine()
{
	phase = ltPhase::IDLE;
	previousPhase =  ltPhase::IDLE;
	callback = [](ltPhase) {};
}

void lt::ltStateMachine::setDetector(std::shared_ptr<lt::Detector> detector)
{
	std::lock_guard<std::mutex> guard{ m };
	this->detector = detector;
}

void lt::ltStateMachine::setTracker(std::function<cv::Ptr<cv::Tracker>()> factoryMethod)
{
	startPreviousState();
	std::lock_guard<std::mutex> guard{ m };
	trackerFactoryMethod = factoryMethod;
	this->tracker = trackerFactoryMethod();
}

void lt::ltStateMachine::startIdle()
{
	previousPhase = phase.load();
	phase = ltPhase::IDLE;
	callback(ltPhase::IDLE);
}

void lt::ltStateMachine::startDetection()
{
	if (detector == nullptr)
		startIdle();

	else
	{
		previousPhase = phase.load();
		phase = ltPhase::DETECTION;
		callback(ltPhase::DETECTION);
	}
		
}

void lt::ltStateMachine::startTracking(cv::InputArray frame, const cv::Rect2d & boundingBox)
{
	if (tracker.empty())
		startIdle();	
	else
	{
		std::lock_guard<std::mutex> guard{ m };
		tracker->init(frame, boundingBox);
		previousPhase = static_cast<lt::ltPhase>(phase);
		phase = ltPhase::TRACKING;
		callback(ltPhase::TRACKING);
	}
}

void lt::ltStateMachine::startPreviousState()
{
	if (previousPhase != ltPhase::TRACKING)
		phase = previousPhase.load();
	else
		phase = ltPhase::IDLE;

	callback(phase);
}

std::vector<cv::Rect> lt::ltStateMachine::step(cv::InputArray frame)
{
	std::lock_guard<std::mutex>{m};
	switch (phase)
	{
		case ltPhase::DETECTION:
			return detector->detect(frame);
			break;

		case ltPhase::TRACKING:
			return trackerPhase(frame);
			break;

		case ltPhase::IDLE:
		default:
			return std::vector<cv::Rect>();
	}
		
}

void lt::ltStateMachine::setPhaseTransitionCallback(phaseTransitionCallback && callback)
{
	this->callback = callback;
}

std::vector<cv::Rect> lt::ltStateMachine::trackerPhase(cv::InputArray frame)
{
	cv::Rect2d trackedObject;
	std::future<std::vector<cv::Rect>> asyncDetect;
	bool detect = false;

	if (detector)
	{
		asyncDetect = std::async([&] {return detector->detect(frame); });
		detect = true;
	}

	bool ok = tracker->update(frame, trackedObject);

	std::vector<cv::Rect> detectedObjects;
	if (detect)
		detectedObjects = asyncDetect.get();
	
	std::vector<cv::Rect> returningVec;

	if (!ok) 
	{	
		if (!detect)
		{
			startPreviousState();
			
		}
		else
		{
			auto minDistance = INT_MAX;
			cv::Rect trackCandidate;
			for (auto &obj : detectedObjects)
			{
				const auto dist = calcDistance(previousTrackedObject, obj);
				if (dist < minDistance)
				{
					minDistance = dist;
					trackCandidate = obj;
				}
				tracker = trackerFactoryMethod();
				tracker->init(frame, trackCandidate);
				trackedObject = trackCandidate;
				std::cout << "retrain \n";
			}
		}

		
		
	}
	else
		returningVec.push_back(trackedObject);
	

	previousTrackedObject = trackedObject;
	return returningVec;
}

int lt::ltStateMachine::calcDistance(const cv::Rect &a, const cv::Rect &b)
{
	if (!a.empty() && !b.empty())
	{
		const auto x = b.x - a.x;
		const auto y = b.y - a.y;

		return ((x*x) + (y*y));
	}
	else
		return -1;
}
