#include "MainController.h"
#include "PixelDegreeTranslator.h"

#include <QApplication>

#include <string>
#include <iostream>
#include <memory>

MainController::MainController(): laser(/*"raspberrypi.local"*/"192.168.1.76",6000)
{
	stateMachine.setPhaseTransitionCallback([&](lt::ltPhase phase) {
		switch (phase)	
		{
		case lt::ltPhase::IDLE:
			std::cout << "transition to IDLE \n";
			break;
		case lt::ltPhase::DETECTION:
			std::cout << "transition to DETECTION \n";
			color = cv::Scalar(0, 0, 255);
			break;
		case lt::ltPhase::TRACKING:
			std::cout << "transition to TRACK \n";
			color = cv::Scalar(0, 255, 0);
			break;
		default:
			break;
		}
	});

	tabBar = new QTabWidget(&window);

	tabBar->addTab(videoInputController.getView(), QApplication::tr("Video"));
	QApplication::connect(&videoInputController, &VideoInputController::videoInputChanges, [this](std::shared_ptr<cv::VideoCapture> cam) {
		camera = cam;
		running = true;
	});

	QApplication::connect(&videoInputController, &VideoInputController::none, [this]() {
		
	});

	tabBar->addTab( detectorController.getView(), QApplication::tr("Detector"));
	QApplication::connect(&detectorController, &DetectorController::detectorChanges, [this](std::shared_ptr<lt::Detector> det) {
		stateMachine.setDetector(det);
		stateMachine.startDetection();
	});
	QApplication::connect(&detectorController, &DetectorController::noneAlgorithm, [this]() {
		stateMachine.startIdle(); //stopDet
	});
	detectorController.getView()->setDisabled(true);

	tabBar->addTab(trackerController.getView(), QApplication::tr("Tracker"));
	QApplication::connect(&trackerController, &TrackerController::trackerChanges, [this](std::function<cv::Ptr<cv::Tracker>()> tracker) {
		stateMachine.setTracker(tracker);
	});
	QApplication::connect(&trackerController, &TrackerController::noneAlgorithm, [this]() {
		stateMachine.startIdle(); //stopTrack
	});
	trackerController.getView()->setDisabled(true);

	QHBoxLayout* mainWindowLayout{ new QHBoxLayout() };
	videoView = new VideoView();
	fpsLabel = new QLabel(&window);

	QApplication::connect(videoView, &VideoView::rectSelected, [this](cv::Rect boundingBox) {
		stateMachine.startTracking(currentFrame, boundingBox);
	});

	mainWindowLayout->addWidget(tabBar);
	mainWindowLayout->addWidget(videoView);
	window.centralWidget()->setLayout(mainWindowLayout);
	window.statusBar()->addWidget(fpsLabel);

	window.show();

}

MainController::~MainController()
{
	running = false;
	if (mainLoopThread.joinable())
		mainLoopThread.join();
}

void MainController::start()
{
	mainLoopThread = std::thread{ [&]() {
		mainLoop();
} };
}

#define SSTR( x ) static_cast< std::ostringstream & >( \
( std::ostringstream() << std::dec << x ) ).str()

void MainController::mainLoop()
{

	lt::PixelDegreeTranslator translator{ 640,480,50,50 };
	
	while (!running);

	if (!camera->isOpened())
	{
		videoView->setDisabled(true);
		std::cerr << "Couldn't open camera \n";
	}

	else
	{
		trackerController.getView()->setDisabled(false);
		detectorController.getView()->setDisabled(false);
	}

	while (running)
	{
		const double timer = (double)cv::getTickCount();
		camera->read(currentFrame);
		if (currentFrame.empty()) {
			std::cerr << "ERROR! blank frame grabbed\n";
			break;
		}
		//cv::GaussianBlur(currentFrame, currentFrame, cv::Size(33, 33), 3, 3);


		auto objects = stateMachine.step(currentFrame);
		
		for (auto& obj : objects)
		{
			cv::rectangle(currentFrame, obj, color);
		}

		if (objects.size() == 1)
		{
			auto roi = objects.front();
			const auto x = roi.x + (roi.width / 2);
			const auto y = roi.y + (roi.height / 2);


			laser.setX(static_cast<unsigned>(translator.invert(translator.calcHorizontalDegreeFromPixel(x))));
			laser.setY(static_cast<unsigned>(translator.calcVerticalDegreeFromPixel(y)));
			std::cout << "roi center " << double(translator.calcHorizontalDegreeFromPixel(x)) << ' ' <<double(translator.calcVerticalDegreeFromPixel(y)) << '\n';
		}

		videoView->showImage(currentFrame);
		const float fps = cv::getTickFrequency() / ((double)cv::getTickCount() - timer);

		QString fpsString{ "FPS: " };
		
		fpsString += QString::fromStdString(std::to_string(fps));
		fpsLabel->setText(fpsString);
	}
	
		
}
