#pragma once
#include "MainComponentTab.h"

#include <opencv2/opencv.hpp>
#include <opencv2/tracking.hpp>

#include <map>
#include <functional>

class TrackerController: public QObject
{
	Q_OBJECT
public:
	TrackerController(QObject* parent = nullptr);
	~TrackerController();

	MainComponentTab* getView() const;

signals:
	void trackerChanges(std::function<cv::Ptr<cv::Tracker>()>);
	void noneAlgorithm();
private:

	MainComponentTab* trackerTab;
	//std::map<int, std::pair<cv::Ptr<cv::Tracker>, const char* >> trackers;
	std::map<int, std::pair<std::function<cv::Ptr<cv::Tracker>()>, const char* >> trackersFactory;
	void createTrackers();
};
