#pragma once
#include "Camera.h"
#include "DetectorGSOC.h"	
#include "MainWindow.h"
#include "VideoView.h"
#include "MainComponentTab.h"
#include "BackgroundsubstractorView.h"
#include "DetectorTab.h"
#include "TrackerTab.h"
#include "ltStateMachine.h"
#include "DetectorController.h"
#include "TrackerController.h"
#include "VideoInputController.h"
#include "LaserConnection.h"

#include <thread>
#include <atomic>

class MainController
{
public:

	MainController();
	~MainController();

	void start();

private:

	MainWindow window;
	QTabWidget* tabBar;
	VideoView* videoView;
	QLabel * fpsLabel;
	//pewnie jeszcze jakies muteksy itp
	std::thread mainLoopThread;
	std::atomic_bool running { false};

	cv::Scalar color{ 0, 0, 255 };
	//lt::Camera camera;
	std::shared_ptr<cv::VideoCapture> camera;
	cv::Mat currentFrame;
	std::shared_ptr<lt::Detector> detector{ nullptr };
	lt::DetectorGSOC detGSOC;
	static constexpr int DEVICE_ID = 0;
	static constexpr int API_ID = cv::CAP_ANY;

	VideoInputController videoInputController;
	DetectorController detectorController;
	TrackerController trackerController;
	lt::ltStateMachine stateMachine;

	lt::LaserConnection laser;

	void mainLoop();
};