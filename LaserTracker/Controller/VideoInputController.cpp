#include "VideoInputController.h"


VideoInputController::VideoInputController(QObject* parent) : QObject(parent)
{
	videoTab = new MainComponentTab();
	videoTab->addAlgorithmView(new QWidget(videoTab), "USB CAMERA");
	videoTab->addAlgorithmView(new QWidget(videoTab), "STREAM");

	connect(videoTab, &MainComponentTab::algorithmChanged, this, [=](int index) {
		std::shared_ptr<cv::VideoCapture> video = std::make_shared<cv::VideoCapture>();
		switch (index)
		{
		case 1:
			video->open(0, cv::CAP_ANY);
			break;
		case 2:
			video->open("udpsrc port=5200 !  application/x-rtp, encoding-name=JPEG,payload=26 !  rtpjpegdepay !"
				"jpegdec ! videoconvert !  appsink",
				cv::CAP_GSTREAMER);			
			break;
		default:
			break;
		}
		emit videoInputChanges(video);
	});

	connect(videoTab, &MainComponentTab::noneAlgorithm, this, [this]() {
		emit none();
	});
}


VideoInputController::~VideoInputController()
{
	videoTab->deleteLater();
}

MainComponentTab * VideoInputController::getView()
{
	return videoTab;
}
