#pragma once
#include "MainComponentTab.h"

#include <opencv2/opencv.hpp>

#include <memory>

class VideoInputController : public QObject
{
	Q_OBJECT
public:
	VideoInputController(QObject* parent = nullptr);
	~VideoInputController();

	MainComponentTab* getView() ;

signals:
	void videoInputChanges(std::shared_ptr<cv::VideoCapture>);
	void none();
private:

	std::shared_ptr<cv::VideoCapture> videoInput;
	MainComponentTab* videoTab;
	//std::map<int, std::pair<cv::Ptr<cv::Tracker>, const char* >> trackers;
};