#pragma once
#include "Detector.h"
#include "DetectorGSOC.h"
#include "MainComponentTab.h"
#include "BackgroundsubstractorView.h"
#include "DetectorTab.h"

#include <QObject>

#include <memory>

class DetectorController : public QObject
{
	Q_OBJECT
public:
	DetectorController(QObject* parent = nullptr);
	~DetectorController();

	MainComponentTab* getView();

signals:
	void detectorChanges(std::shared_ptr<lt::Detector>);
	void noneAlgorithm();
private:

	MainComponentTab* detectorTab;

	BackgroundsubstractorView * gsocView = nullptr;
	std::shared_ptr<lt::DetectorGSOC> detectorGSOC{nullptr};
};