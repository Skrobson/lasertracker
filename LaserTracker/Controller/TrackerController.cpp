#include "TrackerController.h"

TrackerController::TrackerController(QObject* parent):QObject(parent)
{
	trackerTab = new MainComponentTab();
	createTrackers();
	connect(trackerTab, &MainComponentTab::algorithmChanged, this, [=](int index) {
		auto track = trackersFactory.find(index);
		if (track != trackersFactory.end())
		{
			emit trackerChanges(track->second.first);
		}
			
	});

	connect(trackerTab, &MainComponentTab::noneAlgorithm, this, [this]() {
		emit noneAlgorithm();
	});
}

TrackerController::~TrackerController()
{
	if (trackerTab)
		trackerTab->deleteLater();
}

MainComponentTab * TrackerController::getView() const
{
	return trackerTab;
}

void TrackerController::createTrackers()
{
	
	
	std::pair< std::function<cv::Ptr<cv::Tracker>()>, const char* > pair = std::make_pair([]() {return cv::TrackerBoosting::create(); }, "BOOSTING");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	
	pair = std::make_pair([]() {return cv::TrackerMIL::create(); }, "MIL");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	pair = std::make_pair([]() {return  cv::TrackerKCF::create(); }, "KCF");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	pair = std::make_pair([]() {return cv::TrackerTLD::create(); }, "TLD");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	pair = std::make_pair([]() {return cv::TrackerMedianFlow::create(); }, "MEDIANFLOW");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	
	//auto pair = std::make_pair([]() {return  cv::TrackerGOTURN::create();, "GOTURN");
	//trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	pair = std::make_pair([]() {return cv::TrackerMOSSE::create(); }, "MOSSE");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	pair = std::make_pair([]() {return  cv::TrackerCSRT::create(); }, "CSRT");
	trackersFactory.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));
	
	
	
	
	//cv::Ptr<cv::Tracker> tracker;
	//tracker = cv::TrackerBoosting::create();
	//auto pair = std::make_pair(tracker, "BOOSTING");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second),pair));

	//tracker = cv::TrackerMIL::create();
	//pair = std::make_pair(tracker, "MIL");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	//tracker = cv::TrackerKCF::create();
	//pair = std::make_pair(tracker, "KCF");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	//tracker = cv::TrackerTLD::create();
	//pair = std::make_pair(tracker, "TLD");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	//tracker = cv::TrackerMedianFlow::create();
	//pair = std::make_pair(tracker, "MEDIANFLOW");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));

	////tracker = cv::TrackerGOTURN::create();
	////pair = std::make_pair(tracker, "GOTURN");
	////trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));
	//
	//tracker = cv::TrackerMOSSE::create();
	//pair = std::make_pair(tracker, "MOSSE");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));
	//
	//tracker = cv::TrackerCSRT::create();
	//pair = std::make_pair(tracker, "CSRT");
	//trackers.insert(std::make_pair(trackerTab->addAlgorithmView(new QWidget(trackerTab), pair.second), pair));
}
