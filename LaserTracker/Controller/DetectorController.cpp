#include "DetectorController.h"
#include "DetectorDenseOpticalFlow.h"

DetectorController::DetectorController(QObject * parent):QObject(parent)
{
	detectorTab = new DetectorTab();
	gsocView = new BackgroundsubstractorView(detectorTab);
	detectorGSOC = std::make_shared<lt::DetectorGSOC>();

	connect(gsocView, &BackgroundsubstractorView::learningRateChanged,this, [this](double val){
		detectorGSOC->setLearningRate(val);
		std::cout << val << '\n';
		});

	const auto idxGSOC = detectorTab->addAlgorithmView(gsocView, tr("GSOC"));

	connect(detectorTab, &MainComponentTab::algorithmChanged,this, [=](int index) {
		if (index == idxGSOC)
			emit detectorChanges(detectorGSOC);
	});

	const auto idxFarneback = detectorTab->addAlgorithmView(new QWidget(detectorTab), tr("Farneback"));

	connect(detectorTab, &MainComponentTab::algorithmChanged, this, [=](int index) {
		if (index == idxFarneback)
			emit detectorChanges(std::make_shared<lt::DenseOpticalFlowDetector>());
	});

	connect(detectorTab, &MainComponentTab::noneAlgorithm,this, [this]() {
		emit noneAlgorithm();
	});

}

DetectorController::~DetectorController()
{
	detectorTab->deleteLater();
}

MainComponentTab * DetectorController::getView() 
{
	return detectorTab;
}
