#include "PixelDegreeTranslator.h"

#include <iostream>
lt::PixelDegreeTranslator::PixelDegreeTranslator(unsigned width, unsigned height, float horizontalAngle, float verticalAngle):
	width(width),height(height),horizontalAngle(horizontalAngle),verticalAngle(verticalAngle),widthPercent(1.0/width),heightPercent(1.0/height)
	,horizontalTranslationFactor(90.0-(horizontalAngle/2.0)), verticalTranslationFactor(90.0 - (verticalAngle/2.0))
{
}

float lt::PixelDegreeTranslator::calcHorizontalDegreeFromPixel(unsigned x) const
{

	return 	((widthPercent * x) * horizontalAngle) + horizontalTranslationFactor;
}

float lt::PixelDegreeTranslator::calcVerticalDegreeFromPixel(unsigned y) const
{
	std::cout << double(widthPercent) << '\n';
	return 	((heightPercent * y) * verticalAngle) + horizontalTranslationFactor;
}

float lt::PixelDegreeTranslator::invert(float degree) const
{
	return 180.0 - degree;
}
