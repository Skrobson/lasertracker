#include "LaserConnection.h"

#include <iostream>
#include <sstream>

lt::LaserConnection::LaserConnection(): ioService(), socket(ioService)
{

}

lt::LaserConnection::LaserConnection(const std::string& address, unsigned port): LaserConnection()
{
	connect(address, port);
	
}

bool lt::LaserConnection::connect(const std::string & address, unsigned port)
{
	std::cout << "Connecting to laser server: " << address << ':' << port <<'\n';
	//boost::asio::ip::tcp::resolver resolver(ioService);
	//boost::asio::ip::tcp::resolver::query query(address, std::to_string(port));
	//boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);
	//
	//boost::asio::ip::tcp::endpoint endpoint = iter->endpoint();
	//
	//ioThread = std::thread(&LaserConnection::runService, this);

	boost::asio::ip::tcp::endpoint endpoint(
		boost::asio::ip::address::from_string(address),
		port
	);

	try
	{
		socket.connect(endpoint);

		std::cout << "Connected \n";
		return true;
	}
	catch (boost::system::system_error ex)
	{
	
		std::cerr << "Error during connection to " << endpoint.address() << " " << endpoint.port() << '\n';
		std::cerr << ex.what() << '\n';
	}
	
	return false;
}

void lt::LaserConnection::setX(unsigned degree)
{
	std::stringstream stream;
	stream << "x " << degree << '\n';
	sendMessage(stream.str());
}

void lt::LaserConnection::setY(unsigned degree)
{
	std::stringstream stream;
	stream << "y " << degree << '\n';
	sendMessage(stream.str());
}

void lt::LaserConnection::writeHandler(const boost::system::error_code & e, std::size_t size)
{
	if (e)
	{
		//obsloga bledow
	}
}

void lt::LaserConnection::sendMessage(const std::string & message)
{
	boost::asio::async_write(socket, boost::asio::buffer(message.c_str(), message.size()),
		[this](const boost::system::error_code & e, std::size_t size) {
		writeHandler(e, size);
	});
}

void lt::LaserConnection::runService()
{
	try
	{
		ioService.run();
	}
	catch (...)
	{
	}
}
