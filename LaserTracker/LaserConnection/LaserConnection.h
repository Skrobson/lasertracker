#pragma once


#include <boost/asio.hpp>

#include <thread>

namespace lt
{
	class LaserConnection
	{
	public:
		LaserConnection();
		LaserConnection(const std::string& address, unsigned port);

		bool connect(const std::string& address, unsigned port);
		void setX(unsigned degree);
		void setY(unsigned degree);
		//~LaserConnection();

	private:
		boost::asio::io_service ioService;
		boost::asio::ip::tcp::socket socket;

		void writeHandler(const boost::system::error_code& e, std::size_t size);
		void sendMessage(const std::string& message);

		std::thread ioThread;
		void runService();
	};
}
