#pragma once

namespace lt
{
	class PixelDegreeTranslator
	{
	public:

		PixelDegreeTranslator(unsigned width, unsigned height, float horizontalAngle, float verticalAngle);

		float calcHorizontalDegreeFromPixel(unsigned x) const;
		float calcVerticalDegreeFromPixel(unsigned y) const;
		float invert(float degree) const;

	private:
		const unsigned width;
		const unsigned height;

		const double widthPercent;
		const double heightPercent;

		const double horizontalAngle;
		const double verticalAngle;
		const double horizontalTranslationFactor;
		const double verticalTranslationFactor;
	};
}