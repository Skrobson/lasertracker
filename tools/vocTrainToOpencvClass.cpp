#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>



//arg1 input file name
//arg2 out file name
//arg3 dir with img
//arg4 img's suffix, if no jpg will be set
int main(int argc, char** argv)
{
	if (argc < 4)
		return 1;

	std::fstream input{ argv[1], std::fstream::in };
	if (!input.is_open())
		return 2;

	std::fstream outputGood{ std::string(argv[2])+ "good" , std::fstream::out };
	if (!outputGood.is_open())
	{
		input.close();
		return 3;
	}
	
	std::fstream outputBad{ std::string(argv[2])+ "bad", std::fstream::out };
	if (!outputBad.is_open())
	{
		input.close();
		outputBad.close();
		return 4;
	}



	const std::string dir{ argv[3] };
	const std::string suffix = (argc == 5 ? argv[4] : "jpg");

	int p{ 0 }, n{ 0 };

	try 
	{
		while (input.good())
		{
			std::string filename;
			int positive = -1;

			input >> filename ;
			input >> positive;

			auto fullPath = dir + '/' + filename + '.' + suffix;
			if (positive>0)
			{
				cv::Mat img= cv::imread(cv::String(fullPath));
				outputGood << fullPath + " 1 0 0 " + std::to_string(img.size().width) + ' ' + std::to_string(img.size().height) + "\r\n";
				std::cout << "Positive: " + filename + "\r\n";
				++p;
			}
			else
			{
				outputBad << fullPath + "\r\n";
				std::cout << "Negative: " + filename + "\r\n";
				++n;
			}
		}
		
	}
	catch (std::exception e)
	{
		std::cout << e.what();
	}

	std::cout << "Negative samples : " << n << "\r\n";
	std::cout << "Positive samples : " << p << "\r\n";

	input.close();
	outputBad.close();
	outputGood.close();

	return 0;
}