cmake_minimum_required (VERSION 2.6)

include_directories(vocTrainToOpencvClass 
	${OpenCV_INCLUDE_DIRS}
	)

add_executable(vocTrainToOpencvClass
	vocTrainToOpencvClass.cpp
	)

target_link_libraries(vocTrainToOpencvClass
	${OpenCV_LIBS}
	)